# Final Project Plan report

|                  | **Names/Ids**  |
|-----------------:|:---------------|
| *Team ID:*       |                |
| *Team Members:*  |                |
| *Scrum  Mentors:*|                |

**Project Title:**

## 1. Introduction and Background

## 2. Project Plan and Scope

## 3. Brief description of the hardware (Raspberry Pi) and software

## 4. Application

### Description
### Specification

## 5. Design (How to map application onto Hardware and Software)

## 6. Implementation (How to implement the different parts)

## 7. Testing

## 8. Planning and definition of tasks for the individual members

## 9. Conclusion

##  Reference

##   I. Acknowledgement

##   II. Reflection

##   III. Appendix and Glossary

   

   

   
